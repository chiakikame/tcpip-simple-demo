const spawn = require('child_process').spawn;
const path = require('path');
const os = require('os');

const rustServerSourceDir = path.join(__dirname, 'rust-tcp-server');
const executableName = 'tcp-server' + (os.platform() === 'win32' ? '.exe' : '');
const rustServerExecutablePath = path.join(rustServerSourceDir, 'target', 'release', executableName);

console.log('Running cargo...');
const cargoProc = spawn('cargo', ['build', '--release'], {
  cwd: rustServerSourceDir,
  stdio: 'inherit'
});

cargoProc.on('exit', function cargoFinished(code) {
  if (code === 0) {
    console.log('Starting the rust executable... ');
    console.log(`${rustServerExecutablePath}`);
    const rustServerProc = spawn(rustServerExecutablePath, {
      stdio: 'inherit'
    });
    rustServerProc.on('exit', function rustServerExit(code) {
      console.log(`Rust server exited with code ${code}`);
    });
  } else {
    console.log(`Error: cargo exited with code ${code}`);
  }
})
