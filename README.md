# Using TCP/IP protocol inside multiple programming languages.

Here, I demonstrate how to create TCP/IP service (random number generation) in some programming languages.

## Protocol

The transmission uses little endian format.

On connection, the client should send 3 numbers: `count`, `min` and `max` (exclusive) for random number generation service.

The server will respond `n` randomly-generated numbers, where `n` equals to `count` transmitted by client.

After all random numbers are transmitted, the server will close the connection.

## Server implementation

The service is implemented in the following languages:

* JavaScript
* Rust

### JavaScript

To run JavaScript server, use

```sh
node tcp-server.js
```

### Rust

To run Rust server, use

```sh
node run-rust-server.js
```

The `js` script will build the Rust server in `./rust-tcp-server` with `Cargo`, and run the built executable.

## Client implementation

The client is implemented only in JavaScript.

To run it

```sh
node tcp-client.js
```
