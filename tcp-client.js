//
// RNG client
//
const net = require('net');
const address = process.argv[2] || '127.0.0.1';
const port = parseInt(process.argv[3] || "8901");

const client = net.connect(port, address, function connected() {
  console.log('Connected to server at ${address}:${port}, sending data');
  console.log('Requiring 100 random numbers within 10 ~ 32');
  
  const buf = Buffer.alloc(12);
  buf.writeUInt32LE(100, 0);
  buf.writeUInt32LE(10, 4);
  buf.writeUInt32LE(32, 8);
  
  client.write(buf);
});


let buf = null;
let counter = 0;
client.on('data', function dataReceived(data) {
  console.log(`Data of ${data.length} bytes received, processing...`);
  buf = buf ? Buffer.concat([buf, data]) : data;
  console.log(`Concated buffer: ${buf.length} bytes`);
  let i = 0;
  for(i = 0; i < buf.length; i += 4) {
    counter += 1;
    console.log(`[${counter}] decoded: ${buf.readUInt32LE(i)}`);
  }
  buf = buf.slice(i);
});

client.on('end', function connectionLost() {
  console.log('Disconnected from server');
});
