extern crate rand;
extern crate byteorder;

use std::env;
use std::net::{TcpListener, TcpStream, Shutdown};
use std::thread;
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use rand::distributions::{IndependentSample, Range};

const DEFAULT_ADDRESS: &'static str = "127.0.0.1:8901";

fn main() {
    let args: Vec<String> = env::args().collect();
    let address = if args.len() >= 2 {
        args[1].clone()
    } else {
        DEFAULT_ADDRESS.to_string()
    };
    
    match TcpListener::bind(address.clone()) {
        Ok(listener) => {
            start_listening(listener);
        },
        Err(e) => {
            println!("Cannot listen to address {}", address);
            println!("{}", e);
        }
    }
}

fn start_listening(listener: TcpListener) {
    println!("Listening on {}", listener.local_addr().expect("Cannot access local address of listener"));
    for conn_result in listener.incoming() {
        match conn_result {
            Ok(stream) => {
                // If the communication lives long, this approach is not recommended.
                thread::spawn(|| {
                    handle_request(stream);
                });
            },
            Err(e) => {
                println!("Connection error");
                println!("{}", e);
            }
        }
    }
}

fn handle_request(mut stream: TcpStream) {
    let peer_address = stream.peer_addr().expect("Cannot access peer address information");
    let count = stream.read_u32::<LittleEndian>().expect("Cannot read 'count' field");
    let min = stream.read_u32::<LittleEndian>().expect("Cannot read 'min' field");
    let max = stream.read_u32::<LittleEndian>().expect("Cannot read 'max' field");
    let range_uniform = Range::new(min, max);
    let mut rng = rand::thread_rng();
    
    println!("Receiving connection from {}", peer_address);
    println!("Start generating random numbers");
    for i in 1 .. count + 1 {
        let num = range_uniform.ind_sample(&mut rng);
        stream.write_u32::<LittleEndian>(num).expect("Cannot write random number into stream");
        println!("[{}] {}", i, num);
    }
    println!("End generating random numbers");
    println!("Closing connection from {}", peer_address);
    stream.shutdown(Shutdown::Both).expect("Cannot shutdown stream explicitly");
}
