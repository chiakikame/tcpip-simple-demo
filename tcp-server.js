//
// RNG server
//
const net = require('net');
const address = process.argv[2] || '127.0.0.1';
const port = parseInt(process.argv[3] || "8901");

const server = net.createServer(function listener(socket) {
  console.log(`Connection received from ${socket.remoteAddress}:${socket.remotePort} (${socket.remoteFamily})`);
  
  let buf = null;
  
  socket.on('data', function dataReceived(buffer) {
    buf = buf ? Buffer.concat([buf, buffer]) : buffer;
    
    if (buf.length >= 12) {
      const count = buf.readUInt32LE(0);
      const min = buf.readUInt32LE(4);
      const max = buf.readUInt32LE(8);
      const outBuf = Buffer.alloc(4);
      
      console.log(`Count: ${count}, ${min}-${max}`);
      for (let i = 0; i < count; i++) {
        const v = Math.ceil(Math.random() * (max - min)) + min;
        console.log(`[${i + 1}] Emitting ${v}`);
        outBuf.writeUInt32LE(v, 0);
        socket.write(outBuf);
      }
      console.log(`All data written, closing down connection ${socket.remoteAddress}:${socket.remotePort} (${socket.remoteFamily})`);
      socket.end();
    }
  });
});

server.listen(port, address);

server.on('listening', function serverIsListening() {
  const serverInfo = server.address();
  console.log(`Server is established at ${serverInfo.address}:${serverInfo.port} (${serverInfo.family})`);
});
